from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.urls import reverse
from django.views.generic import TemplateView, DetailView, ListView, UpdateView

from accounts.forms import UserCreationForm, UserChangeForm, ProfileChangeForm, PasswordChangeForm
from accounts.models import Profile


class LoginView(TemplateView):
    template_name = 'registration/login.html'

    def get(self, request, *args, **kwargs):
        context = {
            'next': request.GET.get("next")
        }
        return render(request, template_name=self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        context = {}
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(request, username=username, password=password)
        next_page = request.GET.get("next")
        if user is not None:
            login(request, user)
            if next_page:
                return redirect(next_page)
            return redirect("photos_list")
        else:
            context["has_error"] = True
        return render(request, template_name=self.template_name, context=context)


class LogoutView(TemplateView):
    def get(self, request, *args, **kwargs):
        logout(request)
        return redirect("photos_list")


class RegisterView(TemplateView):
    template_name = 'registration/register.html'

    def get(self, request, *args, **kwargs):
        form = UserCreationForm()
        context = {
            'next': request.GET.get("next"),
            'form': form
        }
        return render(request, template_name=self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        form = UserCreationForm(data=request.POST)
        next_page = request.GET.get("next")
        if form.is_valid():
            user = form.save()
            user.save()
            Profile.objects.create(user=user, image='user_pics/default.jpg')
            login(request, user)
            if next_page:
                return redirect(next_page)
            return redirect('photos_list')
        return render(request, template_name=self.template_name, context={'form': form})


class UserProfileView(LoginRequiredMixin, DetailView):
    model = get_user_model()
    template_name = 'profile/profile.html'
    context_object_name = 'user_obj'
    paginate_related_by = 5
    paginate_related_orphans = 0

    def get_context_data(self, **kwargs):
        favorites = self.object.select.all()
        paginator = Paginator(
            favorites, self.paginate_related_by,
            self.paginate_related_orphans
        )
        page_number = self.request.GET.get('page', 1)
        page = paginator.get_page(page_number)
        kwargs['page_obj'] = page
        kwargs['favorites'] = page.object_list
        kwargs['is_paginated'] = page.has_other_pages()
        return super().get_context_data(**kwargs)


class UserProfileUpdateView(UpdateView):
    model = get_user_model()
    form_class = UserChangeForm
    template_name = 'profile/user_profile_update.html'
    context_object_name = 'user_obj'

    def get_object(self, queryset=None):
        return self.model.objects.get(id=self.request.user.id)

    def get_context_data(self, **kwargs):
        if 'profile_form' not in kwargs:
            kwargs['profile_form'] = self.get_profile_form()
        return super(UserProfileUpdateView, self).get_context_data(**kwargs)

    def get_profile_form(self):
        form_kwargs = {'instance': self.object.profile}
        if self.request.method == 'POST':
            form_kwargs['data'] = self.request.POST
            form_kwargs['files'] = self.request.FILES
        return ProfileChangeForm(**form_kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        profile_form = self.get_profile_form()
        if form.is_valid() and profile_form.is_valid():
            return self.form_valid(form, profile_form)
        else:
            return self.form_invalid(form, profile_form)

    def form_invalid(self, form, profile_form):
        context = self.get_context_data(
            form=form, profile_form=profile_form
        )
        return self.render_to_response(context)

    def form_valid(self, form, profile_form):
        response = super().form_valid(form)
        profile_form.save()
        return response

    def get_success_url(self):
        return reverse('profile', kwargs={'pk': self.object.pk})


class ChangePasswordView(UpdateView):
    model = get_user_model()
    template_name = 'registration/change_password.html'
    form_class = PasswordChangeForm
    context_object_name = 'user_obj'

    def get_object(self, queryset=None):
        return self.model.objects.get(id=self.request.user.id)

    def get_success_url(self):
        return reverse('login')

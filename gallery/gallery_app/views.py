from django.contrib.auth.models import User
from django.http import HttpRequest
from django.shortcuts import render, redirect

# Create your views here.
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView

from gallery_app.forms import PhotoForm
from gallery_app.models import Photo


class PhotoListView(ListView):
    template_name = 'photo/photo_list_view.html'
    context_object_name = 'photos'
    model = Photo
    ordering = ('-created_at',)
    paginate_by = 6


class PhotoAddView(CreateView):
    template_name = 'photo/photo_create_view.html'
    model = Photo
    form_class = PhotoForm

    def get_success_url(self):
        return reverse('photos_list')

    def post(self, request: HttpRequest, *args, **kwargs):
        form = self.form_class(data=request.POST, files=request.FILES)
        if form.is_valid():
            photo = form.save(commit=False)
            photo.author = self.request.user
            photo.save()
            return redirect(self.get_success_url())
        return render(request, self.template_name, context={
            'form': form
        })


class PhotoDetailView(DetailView):
    template_name = 'photo/photo_detail_view.html'
    model = Photo
    context_object_name = 'photo'
    extra_context = {
        'users': User.objects.all()
    }


class PhotoUpdateView(UpdateView):
    template_name = 'photo/photo_update_view.html'
    form_class = PhotoForm
    model = Photo
    permission_required = 'gallery_app.change_photo'

    def get_success_url(self):
        return reverse('photo_detail', kwargs={'pk': self.get_object().pk})


class PhotoDeleteView(DeleteView):
    model = Photo
    success_url = reverse_lazy('photos_list')
    permission_required = 'gallery_app.delete_photo'

from django.contrib.auth import get_user_model
from django.db import models


class Photo(models.Model):
    image = models.ImageField(
        null=False, blank=False,
        upload_to='photos', verbose_name='Фотография'
    )
    caption = models.CharField(max_length=200, null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Время создания")
    author = models.ForeignKey(
        get_user_model(), on_delete=models.SET_DEFAULT,
        default=1, related_name='photos',
        verbose_name='Автор'
    )

    def get_favorites(self):
        return Favorites.objects.filter(photo=self).values_list('user', flat=True)


class Favorites(models.Model):
    user = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name='select',
        verbose_name='Пользователь'
    )
    photo = models.ForeignKey(
        'gallery_app.Photo', related_name='select',
        on_delete=models.CASCADE, null=False, blank=False
    )

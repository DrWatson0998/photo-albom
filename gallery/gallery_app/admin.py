from django.contrib import admin
from .models import Photo, Favorites


# Register your models here.
class PhotoAdmin(admin.ModelAdmin):
    list_display = ['id', 'caption', 'created_at', 'author']
    list_filter = ['caption', 'author']
    search_fields = ['caption', 'author']
    fields = ['image', 'caption', 'created_at', 'author', 'favorites']
    readonly_fields = ['created_at']


class FavoritesAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'photo']
    fields = ['user', 'photo']


admin.site.register(Photo, PhotoAdmin)
admin.site.register(Favorites, FavoritesAdmin)


from django.urls import path

from gallery_app.views import (
    PhotoListView, PhotoAddView,
    PhotoDetailView, PhotoUpdateView, PhotoDeleteView
)

urlpatterns = []

photo_urls = [
    path('', PhotoListView.as_view(), name='photos_list'),
    path('photo/add', PhotoAddView.as_view(), name='add_photo'),
    path('photo/<int:pk>/', PhotoDetailView.as_view(), name='photo_detail'),
    path('photo/<int:pk>/delete', PhotoDeleteView.as_view(), name='delete_photo'),
    path('photo/<int:pk>/update', PhotoUpdateView.as_view(), name='photo_update'),
]

urlpatterns += photo_urls

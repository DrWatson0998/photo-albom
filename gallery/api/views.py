from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView

from rest_framework.permissions import (
    IsAuthenticated
)

from gallery_app.models import Photo, Favorites


class FavoriteView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        photo = Photo.objects.get(pk=self.kwargs.get('pk'))
        get_favorite = Favorites.objects.filter(photo_id=photo.pk, user_id=self.request.user.pk)
        if not get_favorite:
            fav = Favorites.objects.create(photo_id=photo.pk, user_id=self.request.user.pk)
            fav.save()
        else:
            get_favorite.delete()
        return JsonResponse({'status': 'ok'})

from rest_framework import serializers

from gallery_app.models import Favorites


class FavoritesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Favorites
        fields = ['id', 'user', 'photo']
        read_only_fields = ['id', ]

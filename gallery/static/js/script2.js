const base_url = 'http://127.0.0.1:8000/api/'

$.ajax({
    url: `${base_url}login/`,
    method: "post",
    data: JSON.stringify({
        username: `${localStorage.getItem("username")}`,
        password: `${localStorage.getItem("password")}`,
    }),
    dataType: "json",
    contentType: "application/json",
}).done(function(data, status) {
    localStorage.setItem("apiToken", data.token);
}).fail(function(response, status) {
    console.log(response)
    console.log(status)
});




$('#favorite').on('click', ()=>{
    let photo = $('#photo')[0].value
    $.ajax({
        url: `${base_url}photo/${photo}/favorite`,
        method: "POST",
        headers: { 
            Authorization: "Token " + localStorage.getItem("apiToken") 
          },
        data: JSON.stringify({
             photo_id: photo 
          }),
        dataType: "json",
        contentType: "application/json",
      }).done(function(data, status) {
          if (data['status'] === 'ok'){
            location.reload(true);
          }
    }).fail(function(response, status) {
        console.log(response)
        console.log(status)
    });
})